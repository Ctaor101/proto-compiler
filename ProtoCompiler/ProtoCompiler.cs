﻿using Ookii.Dialogs.WinForms;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace ProtoCompiler
{
    public partial class ProtoCompiler : Form
    {
        private string ProtoBufPath;


        public ProtoCompiler()
        {
            InitializeComponent();
            SetupDropDown();
            StartPosition = FormStartPosition.CenterScreen;
            MaximizeBox = false;
            FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var openFile = new OpenFileDialog();
            var result = openFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                ProtoBufPath = openFile.FileName;

                tbProtoFile.Text = openFile.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var folderDirectory = new VistaFolderBrowserDialog();
            var result = folderDirectory.ShowDialog();

            if (result == DialogResult.OK)
            {
                tbDestDir.Text = (folderDirectory.SelectedPath);
            }
        }

        private void SetupDropDown()
        {
            cbCompile.Items.Add(new Item("-- Select Type --", "none"));
            cbCompile.Items.Add(new Item("C#", "csharp_out"));
            cbCompile.Items.Add(new Item("Java", "java_out"));
            cbCompile.Items.Add(new Item("Javascript", "js_out"));
            cbCompile.Items.Add(new Item("Objective C", "objc_out"));
            cbCompile.Items.Add(new Item("PHP", "php_out"));
            cbCompile.Items.Add(new Item("Python", "python_out"));
            cbCompile.Items.Add(new Item("Ruby", "ruby_out"));

            cbCompile.SelectedIndex = 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Outputs status 
            rtbOutput.Text = string.Empty;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Validate Input , Return if any fail
            if (CheckProtobuf() && CheckDestination() && CheckType())
            {

                //Get Files Prior too.
                var files = Directory.GetFiles(Path.GetDirectoryName(ProtoBufPath)).ToList();

                //Copy Exe over to file root of protobuf.
                var protocExe = Path.Combine(Environment.CurrentDirectory, "Working", "protoc.exe");
                 var protobufLocation = Path.Combine(Path.GetDirectoryName(ProtoBufPath), "protoc.exe");

                //Copy File to working Directory
                File.Copy(protocExe, protobufLocation);

                //Create the needed directory for google.protobuf common files.                            
                if (!Directory.Exists(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google")))
                {
                    Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google"));

                    // If the destination directory doesn't exist, create it.
                    if (!Directory.Exists(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google","protobuf")))
                        {
                          Directory.CreateDirectory(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google","protobuf"));
                        }
                }

                //Copy Every File from the old location to the new one.

                string[] files2 = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Working", "google", "protobuf"));
                foreach (var file in files2)
                {
                    string temppath = Path.Combine(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google", "protobuf"), Path.GetFileName(file));
                    File.Copy(file, temppath);
                }

                //Process the protobuf compilation and set the stdout to the textbox 
                var args = $"{((Item)cbCompile.SelectedItem).Value} {Path.GetFileName(tbProtoFile.Text)} {Path.GetDirectoryName(ProtoBufPath)}";

                //Run Compilation
                var p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = "Compile.bat";
                p.StartInfo.Arguments = args;
                p.Start();

                var output = p.StandardOutput.ReadToEnd();
                p.WaitForExit(4000);

                rtbOutput.Text = output;
                rtbOutput.Text = string.Concat(rtbOutput.Text, "Process Completed");

                //Now Clean up the mess you made. 
                var files3 = Directory.GetFiles(Path.GetDirectoryName(ProtoBufPath)).ToList();
                                
                foreach (var file in files3)
                {
                    var extension = Path.GetExtension(file);
                    if (extension != ".exe" && extension != ".proto")
                    {
                        if (!files.Contains(file) && Path.GetDirectoryName(ProtoBufPath) != tbDestDir.Text)
                        {
                            //Copy File to Destination
                            File.Copy(file, Path.Combine(tbDestDir.Text, Path.GetFileName(file)), true);
                            Thread.Sleep(2000);
                        }                                         
                    }

                    if (extension == ".exe")
                    {
                        //Delete the protoc.exe
                        File.Delete(file);
                    }
                }

                // If directory does not exist, don't even try   
                if (Directory.Exists(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google")))
                {
                    Directory.Delete(Path.Combine(Path.GetDirectoryName(ProtoBufPath), "google"),true);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (CheckDestination())
            {
                Process.Start(tbDestDir.Text);
            }
        }



        private bool CheckDestination()
        {
            if (string.IsNullOrEmpty(tbDestDir.Text))
            {
                MessageBox.Show("No destination has been entered.");
                return false;
            }

            return true;
        }

        private bool CheckProtobuf()
        {
            if (string.IsNullOrEmpty(tbProtoFile.Text))
            {
                MessageBox.Show("No protobuf file has been selected.");
                return false;
            }

            if (!tbProtoFile.Text.Contains(".proto"))
            {
                MessageBox.Show("Not a valid proto file.");
                return false;
            }

            return true;
        }

        private bool CheckType()
        {
            if (cbCompile.SelectedIndex == 0)
            {
                MessageBox.Show("Please select a compile type.");
                return false;
            }

            return true;
        }
    }
}
