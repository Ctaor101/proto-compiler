echo off
echo .
echo Setting args CompileType:%1 ProtoFile:%2 Working:%3
set arg1=%1
set arg2=%2
set arg3=%3
echo .
echo Changing to Working Directory %3
cd %3 
echo .
echo Starting the compiler
echo .
echo protoc.exe --%1=. --proto_path=./ %2
echo .
echo on
echo off
protoc.exe --%1=. --proto_path=./ %2
echo .
