﻿namespace ProtoCompiler
{
    partial class ProtoCompiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbProtoFIle = new System.Windows.Forms.Label();
            this.lbDestinationDir = new System.Windows.Forms.Label();
            this.tbProtoFile = new System.Windows.Forms.TextBox();
            this.tbDestDir = new System.Windows.Forms.TextBox();
            this.btnChooseFile = new System.Windows.Forms.Button();
            this.btnChooseDir = new System.Windows.Forms.Button();
            this.lbCompileType = new System.Windows.Forms.Label();
            this.cbCompile = new System.Windows.Forms.ComboBox();
            this.lbOutput = new System.Windows.Forms.Label();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnCompile = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnOpenDest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbProtoFIle
            // 
            this.lbProtoFIle.AutoSize = true;
            this.lbProtoFIle.Location = new System.Drawing.Point(45, 25);
            this.lbProtoFIle.Name = "lbProtoFIle";
            this.lbProtoFIle.Size = new System.Drawing.Size(69, 13);
            this.lbProtoFIle.TabIndex = 0;
            this.lbProtoFIle.Text = "Protobuf File:";
            // 
            // lbDestinationDir
            // 
            this.lbDestinationDir.AutoSize = true;
            this.lbDestinationDir.Location = new System.Drawing.Point(6, 56);
            this.lbDestinationDir.Name = "lbDestinationDir";
            this.lbDestinationDir.Size = new System.Drawing.Size(108, 13);
            this.lbDestinationDir.TabIndex = 1;
            this.lbDestinationDir.Text = "Destination Directory:";
            // 
            // tbProtoFile
            // 
            this.tbProtoFile.BackColor = System.Drawing.Color.White;
            this.tbProtoFile.Location = new System.Drawing.Point(117, 22);
            this.tbProtoFile.Name = "tbProtoFile";
            this.tbProtoFile.ReadOnly = true;
            this.tbProtoFile.Size = new System.Drawing.Size(444, 20);
            this.tbProtoFile.TabIndex = 2;
            // 
            // tbDestDir
            // 
            this.tbDestDir.BackColor = System.Drawing.Color.White;
            this.tbDestDir.Location = new System.Drawing.Point(117, 58);
            this.tbDestDir.Name = "tbDestDir";
            this.tbDestDir.ReadOnly = true;
            this.tbDestDir.Size = new System.Drawing.Size(444, 20);
            this.tbDestDir.TabIndex = 3;
            // 
            // btnChooseFile
            // 
            this.btnChooseFile.Location = new System.Drawing.Point(567, 22);
            this.btnChooseFile.Name = "btnChooseFile";
            this.btnChooseFile.Size = new System.Drawing.Size(97, 23);
            this.btnChooseFile.TabIndex = 4;
            this.btnChooseFile.Text = "Choose File";
            this.btnChooseFile.UseVisualStyleBackColor = true;
            this.btnChooseFile.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnChooseDir
            // 
            this.btnChooseDir.Location = new System.Drawing.Point(567, 56);
            this.btnChooseDir.Name = "btnChooseDir";
            this.btnChooseDir.Size = new System.Drawing.Size(97, 23);
            this.btnChooseDir.TabIndex = 5;
            this.btnChooseDir.Text = "Choose Directory";
            this.btnChooseDir.UseVisualStyleBackColor = true;
            this.btnChooseDir.Click += new System.EventHandler(this.button2_Click);
            // 
            // lbCompileType
            // 
            this.lbCompileType.AutoSize = true;
            this.lbCompileType.Location = new System.Drawing.Point(37, 93);
            this.lbCompileType.Name = "lbCompileType";
            this.lbCompileType.Size = new System.Drawing.Size(74, 13);
            this.lbCompileType.TabIndex = 6;
            this.lbCompileType.Text = "Compile Type:";
            // 
            // cbCompile
            // 
            this.cbCompile.FormattingEnabled = true;
            this.cbCompile.Location = new System.Drawing.Point(117, 90);
            this.cbCompile.Name = "cbCompile";
            this.cbCompile.Size = new System.Drawing.Size(121, 21);
            this.cbCompile.TabIndex = 7;
            // 
            // lbOutput
            // 
            this.lbOutput.AutoSize = true;
            this.lbOutput.Location = new System.Drawing.Point(9, 123);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.Size = new System.Drawing.Size(75, 13);
            this.lbOutput.TabIndex = 8;
            this.lbOutput.Text = "Output Status:";
            // 
            // rtbOutput
            // 
            this.rtbOutput.Location = new System.Drawing.Point(12, 148);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(655, 262);
            this.rtbOutput.TabIndex = 9;
            this.rtbOutput.Text = "";
            // 
            // btnCompile
            // 
            this.btnCompile.Location = new System.Drawing.Point(486, 418);
            this.btnCompile.Name = "btnCompile";
            this.btnCompile.Size = new System.Drawing.Size(75, 23);
            this.btnCompile.TabIndex = 10;
            this.btnCompile.Text = "Compile";
            this.btnCompile.UseVisualStyleBackColor = true;
            this.btnCompile.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(577, 418);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnOpenDest
            // 
            this.btnOpenDest.Location = new System.Drawing.Point(567, 93);
            this.btnOpenDest.Name = "btnOpenDest";
            this.btnOpenDest.Size = new System.Drawing.Size(97, 23);
            this.btnOpenDest.TabIndex = 12;
            this.btnOpenDest.Text = "Open Destination";
            this.btnOpenDest.UseVisualStyleBackColor = true;
            this.btnOpenDest.Click += new System.EventHandler(this.button5_Click);
            // 
            // ProtoCompiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 450);
            this.Controls.Add(this.btnOpenDest);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCompile);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.lbOutput);
            this.Controls.Add(this.cbCompile);
            this.Controls.Add(this.lbCompileType);
            this.Controls.Add(this.btnChooseDir);
            this.Controls.Add(this.btnChooseFile);
            this.Controls.Add(this.tbDestDir);
            this.Controls.Add(this.tbProtoFile);
            this.Controls.Add(this.lbDestinationDir);
            this.Controls.Add(this.lbProtoFIle);
            this.Name = "ProtoCompiler";
            this.Text = "Proto Compiler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbProtoFIle;
        private System.Windows.Forms.Label lbDestinationDir;
        private System.Windows.Forms.TextBox tbProtoFile;
        private System.Windows.Forms.TextBox tbDestDir;
        private System.Windows.Forms.Button btnChooseFile;
        private System.Windows.Forms.Button btnChooseDir;
        private System.Windows.Forms.Label lbCompileType;
        private System.Windows.Forms.ComboBox cbCompile;
        private System.Windows.Forms.Label lbOutput;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.Button btnCompile;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnOpenDest;
    }
}

