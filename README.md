# Proto Compiler

A ui wrapped around the not so user friendly Protoc.exe to compile your protobuf files.


## Screenshot

![Screenshot](https://gitlab.com/Ctaor101/proto-compiler/raw/master/examples/screenshot.png)

## Dependencies 

Google.Protobuf.Tools Version:3.6.1 TargetFramework:net461  <br />
Ookii.Dialogs.WinForms Version:1.0.0 TargetFramework:net461
  
## Compiled Version

[Dowload Compiled Version Here](https://gitlab.com/Ctaor101/proto-compiler/raw/master/examples/ProtoCompiler.zip?inline=false)

### By Christopher Taormina