echo off
echo .
echo Setting args CompileType:%1  ProtoFile:%2
set arg1=%1
set arg2=%
echo .
echo Changing to Working Directory
cd Working 
echo .
echo Starting the compiler
echo .
echo protoc.exe --%1=. --proto_path=./ %2
echo .
echo on
echo off
protoc.exe --%1=. --proto_path=./ %2
echo .
